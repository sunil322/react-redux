import React, { Component } from "react";
import "./App.css";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import Products from "./components/Products";
import { Route, Routes } from "react-router-dom";
import AddProduct from "./components/AddProduct";
import axios from "axios";
import Loader from "./components/Loader";
import Error from "./components/Error";
import UpdateProduct from "./components/UpdateProduct";
import NoPageFound from "./components/NoPageFound";
import { connect } from "react-redux";
import { INIT_PRODUCTS } from "./redux/actionTypes";
import AddToCart from "./components/AddToCart";

class App extends Component {
    constructor(props) {
        super(props);

        this.API_STATES = {
            LOADING: "loading",
            LOADED: "loaded",
            ERROR: "error",
        };

        this.state = {
            status: this.API_STATES.LOADING,
            productToBeEdited: null,
        };
    }

    fetchData() {
        this.setState(
            {
                status: this.API_STATES.LOADING,
            },
            () => {
                axios
                    .get("https://fakestoreapi.com/products")
                    .then((res) => {
                        this.props.initialProductsHandler(res.data);
                        this.setState({
                            status: this.API_STATES.LOADED,
                        });
                    })
                    .catch((err) => {
                        this.setState({
                            status: this.API_STATES.ERROR,
                        });
                    });
            }
        );
    }

    productToEdit = (id) => {
        let productToBeEdited = this.props.products.find((product) => {
            return product.id === id;
        });
        this.setState((prevState) => {
            return {
                ...prevState,
                productToBeEdited,
            };
        });
    };

    componentDidMount() {
        this.fetchData();
    }

    render() {
        return (
            <div className="main-container">
                <Navbar />
                {this.state.status === this.API_STATES.LOADING && <Loader />}
                {this.state.status === this.API_STATES.ERROR && <Error />}
                {this.state.status === this.API_STATES.LOADED &&
                    this.props.products.length === 0 && (
                        <h1 style={{ textAlign: "center" }}>
                            No Product Found
                        </h1>
                    )}
                {this.state.status === this.API_STATES.LOADED &&
                    this.props.products.length > 0 && (
                        <Routes>
                            <Route
                                path="/"
                                element={
                                    <Products
                                        products={this.props.products}
                                        productToEdit={this.productToEdit}
                                    />
                                }
                            />
                            <Route
                                path="/add-product"
                                element={<AddProduct />}
                            />
                            <Route
                                path="/update-product/:id"
                                element={
                                    this.state.productToBeEdited !== null && (
                                        <UpdateProduct
                                            product={
                                                this.state.productToBeEdited
                                            }
                                            updateProduct={this.updateProduct}
                                        />
                                    )
                                }
                            />
                            <Route path="/cart" element={<AddToCart />} />
                            <Route path="*" element={<NoPageFound />} />
                        </Routes>
                    )}
                <Footer />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const products = state.productsReducer.products;
    return {
        products,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        initialProductsHandler: (products) =>
            dispatch({
                type: INIT_PRODUCTS,
                payload: products,
            }),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
