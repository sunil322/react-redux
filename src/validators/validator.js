const titleValidator = (title) => {
    if (title === "") {
        return "Title required";
    } else if (title.length < 5 || title.length > 100) {
        return "Title length should be between 5-100 characters";
    } else {
        return "";
    }
};

const descriptionValidator = (description) => {
    if (description === "") {
        return "Description required";
    } else if (description.length < 10 || description.length > 1000) {
        return "Description length should be between 10-1000 characters";
    } else {
        return "";
    }
};
const priceValidator = (price) => {
    if (price === "") {
        return "Price required";
    } else if (isNaN(Number(price))) {
        return "Price should be number only";
    } else if (Number(price) <= 0) {
        return "Price can't be zero or negative";
    } else {
        return "";
    }
};

const categoryValidator = (category) => {
    if (category === "") {
        return "Select Category";
    } else {
        return "";
    }
};

const imageUrlValidator = (imageUrl) => {
    if (imageUrl === "") {
        return "Image url required";
    } else if (imageUrl.startsWith("https") || imageUrl.startsWith("http")) {
        return "";
    } else {
        return "Invalid url";
    }
};
const ratingValidator = (rating) => {
    if (rating === "") {
        return "Rating required";
    } else if (Number(rating) <= 0 || Number(rating) > 5) {
        return "Rating should be between 1-5";
    } else {
        return "";
    }
};
const countValidator = (count) => {
    if (count === "") {
        return "Count value required";
    } else if (Number(count) <= 0) {
        return "Count can't be zero or negative";
    } else {
        return "";
    }
};

export {
    titleValidator,
    descriptionValidator,
    priceValidator,
    categoryValidator,
    imageUrlValidator,
    ratingValidator,
    countValidator,
};
