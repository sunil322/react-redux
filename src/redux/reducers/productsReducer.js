import {
    ADD_PRODUCT,
    REMOVE_PRODUCT,
    UPDATE_PRODUCT,
    INIT_PRODUCTS,
} from "../actionTypes";

const initialState = {
    products: [],
};

export default function productsReducer(state = initialState, action) {
    switch (action.type) {
        case INIT_PRODUCTS:
            const products = action.payload;
            return {
                products: products,
            };
        case ADD_PRODUCT:
            const product = action.payload;
            return {
                products: [...state.products, product],
            };
        case REMOVE_PRODUCT:
            const id = action.payload;
            return {
                products: state.products.filter((product) => {
                    return product.id !== id;
                }),
            };
        case UPDATE_PRODUCT:
            const productData = action.payload;
            return {
                products: state.products.map((product) => {
                    if (productData.id === product.id) {
                        return productData;
                    } else {
                        return product;
                    }
                }),
            };
        default:
            return state;
    }
}
