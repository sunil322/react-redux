import { ADD_TO_CART, REMOVE_FROM_CART, CHANGE_QUANTITY } from "../actionTypes";

const initialState = {
    cartItems: [],
};

export default function cartReducer(state = initialState, action) {
    switch (action.type) {
        case ADD_TO_CART:
            const cartProduct = action.payload;
            return {
                cartItems: [cartProduct, ...state.cartItems],
            };
        case REMOVE_FROM_CART:
            const cartItemId = action.payload;
            return {
                cartItems: state.cartItems.filter((cartItem) => {
                    return cartItem.id !== cartItemId;
                }),
            };
        case CHANGE_QUANTITY:
            const { productId, quantity } = action.payload;
            return {
                cartItems: state.cartItems.map((cartItem) => {
                    if (cartItem.id === productId) {
                        return {
                            ...cartItem,
                            quantity: cartItem.quantity + quantity,
                        };
                    } else {
                        return cartItem;
                    }
                }),
            };
        default:
            return state;
    }
}
