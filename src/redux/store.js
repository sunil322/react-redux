import { configureStore } from "@reduxjs/toolkit";
import productsReducer from "../redux/reducers/productsReducer";
import cartReducer from "./reducers/cartReducer";

const store = configureStore({
    reducer: { productsReducer, cartReducer },
});

export default store;
