import React, { Component } from "react";
import "./Navbar.css";
import logo from "../images/logo.png";
import { Link, NavLink } from "react-router-dom";
import { connect } from "react-redux";

class Navbar extends Component {
    render() {
        return (
            <>
                <nav>
                    <div className="nav-logo">
                        <Link to="/">
                            <img src={logo} alt="logo" />
                        </Link>
                    </div>
                    <div className="nav-menu">
                        <ul>
                            <li className="home-link">
                                <NavLink to="/">Home</NavLink>
                            </li>
                            <li>
                                <NavLink to="/add-product">Add Product</NavLink>
                            </li>
                            <li className="add-to-cart">
                                <Link to="/cart">
                                    <i className="fa-solid fa-cart-plus"></i>
                                    {this.props.cartItems.length === 0 ? (
                                        <></>
                                    ) : (
                                        <span>
                                            {this.props.cartItems.length}
                                        </span>
                                    )}
                                </Link>
                            </li>
                        </ul>
                    </div>
                </nav>
            </>
        );
    }
}

const mapStateToProps = (state) => {
    const cartItems = state.cartReducer.cartItems;
    return {
        cartItems,
    };
};

export default connect(mapStateToProps, null)(Navbar);
