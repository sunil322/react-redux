import React, { Component } from "react";
import { connect } from "react-redux";
import { CHANGE_QUANTITY, REMOVE_FROM_CART } from "../redux/actionTypes";

class CartItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            clickOnRemove: false,
        };
    }

    confirmRemove = () => {
        this.props.removeFromCartHandler(this.props.cartItem.id);
        this.setState({
            clickOnRemove: false,
        });
    };

    cancelRemove = () => {
        this.setState({
            clickOnRemove: false,
        });
    };

    render() {
        const { id, title, image, quantity } = this.props.cartItem;

        const cartItem = this.props.cartItems.find((cartItem) => {
            return cartItem.id === id;
        });

        return (
            <div className="cart-item">
                <div className="product-div">
                    <div className="product-image">
                        <img src={image} alt="product-img" />
                    </div>
                    <div className="product-info">
                        <h4>{title}</h4>
                        <h3>
                            Price : ${" "}
                            {Number(cartItem.price) * cartItem.quantity}
                        </h3>
                        {this.state.clickOnRemove ? (
                            <div className="button-toggle">
                                <span>
                                    <i
                                        className="fa-sharp fa-solid fa-trash"
                                        onClick={this.confirmRemove}
                                    ></i>
                                </span>
                                <span>
                                    <i
                                        className="fa-solid fa-xmark"
                                        onClick={this.cancelRemove}
                                    ></i>
                                </span>
                            </div>
                        ) : (
                            <button
                                onClick={() =>
                                    this.setState({ clickOnRemove: true })
                                }
                            >
                                REMOVE
                            </button>
                        )}
                    </div>
                </div>
                <div className="quanity-div">
                    <button
                        disabled={
                            Number(cartItem.quantity) === 5 ? true : false
                        }
                        onClick={() =>
                            this.props.changeQuantityHandler({
                                productId: id,
                                quantity: 1,
                            })
                        }
                    >
                        <i className="fa-solid fa-chevron-up"></i>
                    </button>
                    <span>{quantity}</span>
                    <button
                        disabled={
                            Number(cartItem.quantity) === 1 ? true : false
                        }
                        onClick={() =>
                            this.props.changeQuantityHandler({
                                productId: id,
                                quantity: -1,
                            })
                        }
                    >
                        <i className="fa-solid fa-chevron-down"></i>
                    </button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const cartItems = state.cartReducer.cartItems;
    return {
        cartItems,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        removeFromCartHandler: (productId) =>
            dispatch({
                type: REMOVE_FROM_CART,
                payload: productId,
            }),
        changeQuantityHandler: (productInfo) => {
            dispatch({
                type: CHANGE_QUANTITY,
                payload: productInfo,
            });
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartItem);
