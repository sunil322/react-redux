import React, { Component } from "react";
import "./Products.css";
import Product from "./Product";
import { connect } from "react-redux";
import { REMOVE_PRODUCT } from "../redux/actionTypes";

class Products extends Component {
    deleteProduct = (id) => {
        this.props.deleteProductHandler(id);
    };
    productToEdit = (id) => {
        this.props.productToEdit(id);
    };
    render() {
        return (
            <div className="products-container">
                {this.props.products.map((product) => {
                    return (
                        <Product
                            key={product.id}
                            product={product}
                            deleteProduct={this.deleteProduct}
                            productToEdit={this.productToEdit}
                        />
                    );
                })}
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        deleteProductHandler: (productId) =>
            dispatch({
                type: REMOVE_PRODUCT,
                payload: productId,
            }),
    };
};

export default connect(null, mapDispatchToProps)(Products);
