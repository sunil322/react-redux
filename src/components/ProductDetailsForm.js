import React, { Component } from "react";
import "./ProductDetailsForm.css";
import {
    titleValidator,
    descriptionValidator,
    categoryValidator,
    imageUrlValidator,
    priceValidator,
    ratingValidator,
    countValidator,
} from "../validators/validator";

class ProductDetailsForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "",
            description: "",
            category: "",
            price: "",
            image: "",
            rate: "",
            count: "",
            isAdded: "",
            errors: {},
        };
    }

    handleOnChangeEvent = (event) => {
        const { name, value } = event.target;
        this.setState((prevState) => {
            return {
                ...prevState,
                [name]: value,
            };
        });
    };

    handleSubmitEvent = (event) => {
        event.preventDefault();

        let { title, description, category, price, image, rate, count } =
            this.state;

        title = title.trim();
        description = description.trim();
        category = category.trim();
        image = image.trim();

        let allErrors = {};

        const updateError = (fieldName, message) => {
            allErrors[fieldName] = message;
        };

        titleValidator(title) !== "" &&
            updateError("title", titleValidator(title));
        descriptionValidator(description) !== "" &&
            updateError("description", descriptionValidator(description));
        categoryValidator(category) !== "" &&
            updateError("category", categoryValidator(category));
        imageUrlValidator(image) !== "" &&
            updateError("image", imageUrlValidator(image));
        priceValidator(price) !== "" &&
            updateError("price", priceValidator(price));
        ratingValidator(rate) !== "" &&
            updateError("rate", ratingValidator(rate));
        countValidator(count) !== "" &&
            updateError("count", countValidator(count));

        if (Object.keys(allErrors).length === 0) {
            this.props.addProductData({
                title,
                description,
                category,
                price,
                image,
                rate,
                count,
            });

            this.setState({
                title: "",
                description: "",
                category: "",
                price: "",
                image: "",
                rate: "",
                count: "",
                errors: {},
                isAdded: true,
            });
            setTimeout(() => {
                this.setState({
                    isAdded: false,
                });
            }, 3000);
        } else {
            this.setState((prevState) => {
                return {
                    ...prevState,
                    errors: allErrors,
                };
            });
        }
    };

    render() {
        return (
            <div className="form-container">
                <form onSubmit={(event) => this.handleSubmitEvent(event)}>
                    {this.state.isAdded && (
                        <div className="message">
                            <p>Product Added successfully</p>
                        </div>
                    )}
                    <legend>{this.props.header}</legend>
                    <div className="form-field">
                        <label className="label" htmlFor="title">
                            Title
                        </label>
                        <input
                            type="text"
                            id="title"
                            className="input"
                            name="title"
                            onChange={this.handleOnChangeEvent}
                            value={this.state.title}
                        />
                    </div>
                    {this.state.errors.title ? (
                        <p className="error">{this.state.errors.title}</p>
                    ) : (
                        <p style={{ visibility: "hidden" }}>""</p>
                    )}
                    <div className="form-field">
                        <label className="label" htmlFor="description">
                            Description
                        </label>
                        <textarea
                            rows={4}
                            id="description"
                            className="input"
                            name="description"
                            onChange={this.handleOnChangeEvent}
                            value={this.state.description}
                        />
                    </div>
                    {this.state.errors.description ? (
                        <p className="error">{this.state.errors.description}</p>
                    ) : (
                        <p style={{ visibility: "hidden" }}>""</p>
                    )}
                    <div className="form-field">
                        <label className="label" htmlFor="category">
                            Category
                        </label>
                        <select
                            className="input"
                            id="category"
                            name="category"
                            onChange={this.handleOnChangeEvent}
                            value={this.state.category}
                        >
                            <option value="">Select Category</option>
                            <option value="men's clothing">
                                Men's Clothing
                            </option>
                            <option value="women's clothing">
                                Women's Clothing
                            </option>
                            <option value="electronics">Electronics</option>
                            <option value="jewelery">Jewelery</option>
                        </select>
                    </div>
                    {this.state.errors.category ? (
                        <p className="error">{this.state.errors.category}</p>
                    ) : (
                        <p style={{ visibility: "hidden" }}>""</p>
                    )}
                    <div className="form-field">
                        <label className="label" htmlFor="price">
                            Price
                        </label>
                        <input
                            type="text"
                            id="price"
                            className="input"
                            name="price"
                            onChange={this.handleOnChangeEvent}
                            value={this.state.price}
                        />
                    </div>
                    {this.state.errors.price ? (
                        <p className="error">{this.state.errors.price}</p>
                    ) : (
                        <p style={{ visibility: "hidden" }}>""</p>
                    )}
                    <div className="form-field">
                        <label className="label" htmlFor="url">
                            Product image url
                        </label>
                        <input
                            type="text"
                            className="input"
                            id="url"
                            name="image"
                            onChange={this.handleOnChangeEvent}
                            value={this.state.image}
                        />
                    </div>
                    {this.state.errors.image ? (
                        <p className="error">{this.state.errors.image}</p>
                    ) : (
                        <p style={{ visibility: "hidden" }}>""</p>
                    )}
                    <div className="form-field">
                        <label htmlFor="rate" className="label">
                            Rating
                        </label>
                        <input
                            type="number"
                            id="rate"
                            name="rate"
                            max="5"
                            className="input"
                            onChange={this.handleOnChangeEvent}
                            value={this.state.rate}
                        />
                    </div>
                    {this.state.errors.rate ? (
                        <p className="error">{this.state.errors.rate}</p>
                    ) : (
                        <p style={{ visibility: "hidden" }}>""</p>
                    )}
                    <div className="form-field">
                        <label htmlFor="count" className="label">
                            Count
                        </label>
                        <input
                            type="number"
                            min="0"
                            id="count"
                            name="count"
                            className="input"
                            onChange={this.handleOnChangeEvent}
                            value={this.state.count}
                        />
                    </div>
                    {this.state.errors.count ? (
                        <p className="error">{this.state.errors.count}</p>
                    ) : (
                        <p style={{ visibility: "hidden" }}>""</p>
                    )}
                    <div className="btn-container">
                        <button>
                            <span>{this.props.buttonText}</span>
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}
export default ProductDetailsForm;
