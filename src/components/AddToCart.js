import React, { Component } from "react";
import CartItem from "./CartItem";
import "./AddToCart.css";
import { connect } from "react-redux";
import EmptyCart from "./EmptyCart";

class AddToCart extends Component {
    render() {
        const cartItems = this.props.cartItems;
        const totalPrice = cartItems.reduce((totalPrice, cartItem) => {
            return totalPrice + Number(cartItem.price) * cartItem.quantity;
        }, 0);
        return (
            <div className="add-to-cart-container">
                <div className="cart-item-list">
                    {cartItems.length === 0 ? (
                        <EmptyCart />
                    ) : (
                        cartItems.map((cartItem) => {
                            return (
                                <CartItem
                                    key={cartItem.id}
                                    cartItem={cartItem}
                                />
                            );
                        })
                    )}
                    <div className="order-price-div">
                        {this.props.cartItems.length !== 0 && (
                            <>
                                <h3>TOTAL PRICE : $ {totalPrice.toFixed(2)}</h3>
                                <button>CHECKOUT</button>
                            </>
                        )}
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const cartItems = state.cartReducer.cartItems;
    return {
        cartItems,
    };
};

export default connect(mapStateToProps, null)(AddToCart);
