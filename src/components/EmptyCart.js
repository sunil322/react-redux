import React, { Component } from "react";
import "./EmptyCart.css";
import { Link } from "react-router-dom";

class EmptyCart extends Component {
    render() {
        return (
            <div className="empty-cart-div">
                <h1>Your cart is empty</h1>
                <div className="browse-product">
                    <Link to="/">
                        <button>Browse products</button>
                    </Link>
                </div>
            </div>
        );
    }
}

export default EmptyCart;
