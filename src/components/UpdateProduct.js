import React, { Component } from "react";
import "./ProductDetailsForm.css";
import { connect } from "react-redux";
import { UPDATE_PRODUCT } from "../redux/actionTypes";
import {
    titleValidator,
    descriptionValidator,
    categoryValidator,
    imageUrlValidator,
    priceValidator,
} from "../validators/validator";

class UpdateProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "",
            description: "",
            category: "",
            price: "",
            image: "",
            errors: {},
            isUpdated: "",
        };
    }

    handleOnChangeEvent = (event) => {
        const { name, value } = event.target;
        this.setState((prevState) => {
            return {
                ...prevState,
                [name]: value,
            };
        });
    };
    componentDidMount() {
        this.setState((prevState) => {
            return {
                ...prevState,
                ...this.props.product,
            };
        });
    }

    handleSubmitEvent = (event) => {
        event.preventDefault();

        let {
            id,
            title,
            description,
            category,
            price,
            image,
            rating: { rate, count },
        } = this.state;

        title = title.trim();
        description = description.trim();
        category = category.trim();
        image = image.trim();

        let allErrors = {};

        const updateError = (fieldName, message) => {
            allErrors[fieldName] = message;
        };

        titleValidator(title) !== "" &&
            updateError("title", titleValidator(title));
        descriptionValidator(description) !== "" &&
            updateError("description", descriptionValidator(description));
        categoryValidator(category) !== "" &&
            updateError("category", categoryValidator(category));
        imageUrlValidator(image) !== "" &&
            updateError("image", imageUrlValidator(image));
        priceValidator(price) !== "" &&
            updateError("price", priceValidator(price));

        if (Object.keys(allErrors).length === 0) {
            this.props.updateProductHandler({
                id,
                title,
                price,
                image,
                category,
                description,
                rating: {
                    rate,
                    count,
                },
            });
            this.setState({
                isUpdated: true,
            });
            setTimeout(() => {
                this.setState({
                    isUpdated: false,
                });
            }, 3000);
        } else {
            this.setState((prevState) => {
                return {
                    ...prevState,
                    errors: allErrors,
                };
            });
        }
    };

    render() {
        return (
            <div className="form-container">
                <form onSubmit={(event) => this.handleSubmitEvent(event)}>
                    {this.state.isUpdated && (
                        <div className="message">
                            <p>Product updated successfully</p>
                        </div>
                    )}
                    <legend>Update Information</legend>
                    <div className="form-field">
                        <label className="label" htmlFor="title">
                            Title
                        </label>
                        <input
                            type="text"
                            id="title"
                            className="input"
                            name="title"
                            onChange={this.handleOnChangeEvent}
                            defaultValue={this.props.product.title}
                        />
                    </div>
                    {this.state.errors.title ? (
                        <p className="error">{this.state.errors.title}</p>
                    ) : (
                        <p style={{ visibility: "hidden" }}>""</p>
                    )}
                    <div className="form-field">
                        <label className="label" htmlFor="description">
                            Description
                        </label>
                        <textarea
                            rows={4}
                            id="description"
                            className="input"
                            name="description"
                            onChange={this.handleOnChangeEvent}
                            defaultValue={this.props.product.description}
                        />
                    </div>
                    {this.state.errors.description ? (
                        <p className="error">{this.state.errors.description}</p>
                    ) : (
                        <p style={{ visibility: "hidden" }}>""</p>
                    )}
                    <div className="form-field">
                        <label className="label" htmlFor="category">
                            Category
                        </label>
                        <select
                            className="input"
                            id="category"
                            name="category"
                            onChange={this.handleOnChangeEvent}
                            defaultValue={this.props.product.category}
                        >
                            <option value="">Select Category</option>
                            <option value="men's clothing">
                                Men's Clothing
                            </option>
                            <option value="women's clothing">
                                Women's Clothing
                            </option>
                            <option value="electronics">Electronics</option>
                            <option value="jewelery">Jewelery</option>
                        </select>
                    </div>
                    {this.state.errors.category ? (
                        <p className="error">{this.state.errors.category}</p>
                    ) : (
                        <p style={{ visibility: "hidden" }}>""</p>
                    )}
                    <div className="form-field">
                        <label className="label" htmlFor="price">
                            Price
                        </label>
                        <input
                            type="text"
                            id="price"
                            className="input"
                            name="price"
                            onChange={this.handleOnChangeEvent}
                            defaultValue={this.props.product.price}
                        />
                    </div>
                    {this.state.errors.price ? (
                        <p className="error">{this.state.errors.price}</p>
                    ) : (
                        <p style={{ visibility: "hidden" }}>""</p>
                    )}
                    <div className="form-field">
                        <label className="label" htmlFor="url">
                            Product image url
                        </label>
                        <input
                            type="text"
                            className="input"
                            id="url"
                            name="image"
                            onChange={this.handleOnChangeEvent}
                            defaultValue={this.props.product.image}
                        />
                    </div>
                    {this.state.errors.image ? (
                        <p className="error">{this.state.errors.image}</p>
                    ) : (
                        <p style={{ visibility: "hidden" }}>""</p>
                    )}
                    <div className="btn-container">
                        <button>
                            <span>UPDATE PRODUCT</span>
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateProductHandler: (product) =>
            dispatch({
                type: UPDATE_PRODUCT,
                payload: product,
            }),
    };
};

export default connect(null, mapDispatchToProps)(UpdateProduct);
