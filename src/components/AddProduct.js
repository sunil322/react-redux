import React, { Component } from "react";
import ProductDetailsForm from "./ProductDetailsForm";
import { connect } from "react-redux";
import { ADD_PRODUCT } from "../redux/actionTypes";
import { v4 as uuidv4 } from "uuid";

class AddProduct extends Component {
    addProductData = (product) => {
        const { title, description, category, image, price, rate, count } =
            product;
        this.props.addProductHandler({
            id: uuidv4(),
            title,
            description,
            category,
            image,
            price,
            rating: {
                rate,
                count,
            },
        });
    };

    render() {
        return (
            <ProductDetailsForm
                addProductData={this.addProductData}
                header={"Add Product Information"}
                buttonText={"ADD PRODUCT"}
            />
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addProductHandler: (product) =>
            dispatch({
                type: ADD_PRODUCT,
                payload: product,
            }),
    };
};

export default connect(null, mapDispatchToProps)(AddProduct);
