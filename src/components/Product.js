import React, { Component } from "react";
import "./Products.css";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { ADD_TO_CART } from "../redux/actionTypes";

class Product extends Component {
    constructor(props) {
        super(props);
        this.state = {
            addedToCart: false,
            existInCart: false,
            clickedRemoveButton: false,
        };
    }

    handleCancelButton = () => {
        this.setState({
            clickedRemoveButton: false,
        });
    };

    handleConfirmButton = () => {
        this.props.deleteProduct(this.props.product.id);
        this.setState({
            clickedRemoveButton: false,
        });
    };

    deleteProduct = (id) => {
        this.setState({
            clickedRemoveButton: true,
        });
    };

    productToEdit = (id) => {
        this.props.productToEdit(id);
    };

    cartClickHandler = (id, title, price, image) => {
        
        let alreadyExistInCart = this.props.cartItems.find((cartItem) => {
            return cartItem.id === id;
        });

        if (alreadyExistInCart === undefined) {
            this.setState((prevState) => {
                return {
                    ...prevState,
                    addedToCart: true,
                    existInCart: false,
                };
            });
            this.props.addToCartHandler({
                id,
                title,
                image,
                price,
                quantity: 1,
            });
            setTimeout(() => {
                this.setState((prevState) => {
                    return {
                        ...prevState,
                        addedToCart: false,
                    };
                });
            }, 3000);
        } else {
            this.setState((prevState) => {
                return {
                    ...prevState,
                    addedToCart: false,
                    existInCart: true,
                };
            });
            setTimeout(() => {
                this.setState((prevState) => {
                    return {
                        ...prevState,
                        existInCart: false,
                    };
                });
            }, 3000);
        }
    };

    render() {
        const { id, title, category, description, image, price, rating } =
            this.props.product;
        return (
            <div className="product">
                <i
                    className="fa-sharp fa-solid fa-cart-plus cart-button"
                    onClick={() =>
                        this.cartClickHandler(id, title, price, image)
                    }
                ></i>
                {this.state.addedToCart && (
                    <p
                        className="cart-message"
                        style={{ backgroundColor: "rgb(0,0,0)" }}
                    >
                        Added to the cart successfully
                    </p>
                )}
                {this.state.existInCart && (
                    <p
                        className="cart-message"
                        style={{ backgroundColor: "rgb(0, 0, 0)" }}
                    >
                        Already exists in the cart
                    </p>
                )}
                <div className="product-img">
                    <img src={image} alt="product" />
                </div>
                <div className="product-info">
                    <p className="category">{category}</p>
                    <p className="title">{title}</p>
                    <p className="description">{description}</p>
                    <p className="price">
                        Price : <span>${price}</span>
                    </p>
                    <p className="rating">
                        <span>
                            <i className="fa-solid fa-star"></i> {rating.rate} (
                            {rating.count})
                        </span>
                    </p>
                </div>
                <div className="button-div">
                    <Link to={`/update-product/${id}`}>
                        <button
                            className="update-btn"
                            onClick={() => this.productToEdit(id)}
                        >
                            <i className="fa-solid fa-pen-to-square"></i>
                            <span>UPDATE</span>
                        </button>
                    </Link>
                    {this.state.clickedRemoveButton ? (
                        <div className="button-toggle-div">
                            <button
                                className="confirm-btn"
                                onClick={this.handleConfirmButton}
                            >
                                CONFIRM
                            </button>
                            <button
                                className="cancel-btn"
                                onClick={this.handleCancelButton}
                            >
                                CANCEL
                            </button>
                        </div>
                    ) : (
                        <button
                            className="delete-btn"
                            onClick={() => this.deleteProduct(id)}
                        >
                            <i className="fa-sharp fa-solid fa-trash"></i>
                            <span>DELETE</span>
                        </button>
                    )}
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    const cartItems = state.cartReducer.cartItems;
    return {
        cartItems,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        addToCartHandler: (product) => {
            dispatch({
                type: ADD_TO_CART,
                payload: product,
            });
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Product);
