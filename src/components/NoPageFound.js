import React, { Component } from "react";
import "./NoPageFound.css";

class NoPageFound extends Component {
    render() {
        return (
            <div className="no-page-found-div">
                <h2>404</h2>
                <p>The Page you are looking for doesn't exist</p>
            </div>
        );
    }
}

export default NoPageFound;
