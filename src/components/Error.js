import React, { Component } from "react";
import "./Error.css";

class Error extends Component {
    render() {
        return (
            <div className="error-div">
                <h1>500 Internal Server Error</h1>
                <h2>Error in Loading Products</h2>
            </div>
        );
    }
}

export default Error;
